from cProfile import run
from logging import exception
import os
import telebot
import time


API_KEY = "5300897030:AAGvo2TrTnw54cA1OvxOhEgtAiuEh4FTTX4"
bot = telebot.TeleBot(API_KEY)

@bot.message_handler(commands=['start','Boa','Salve',"oi", "ola", "olá", "opa", "salve", "suave?", "opa e ae?","alo", "alô", "fala ai", "fala aí", "fala", 'oii', 'amor', 'oi amor', 'oi gostoso', 'oi indo', 'chuchu'])
def boa(message):
    bot.reply_to(message, "Olá tudo bem?," ' ' "Como posso ajudar?" ' ' 'Para saber mais comandos digite /help')  

@bot.message_handler(commands=["Cotação", 'cotacao', 'cotação', 'cotaçao', 'Cotacao', 'cotacão', 'Cotacão'])
def cotacao(message):
    bot.reply_to(message, "Qual cotação você gostaria?;" ' ' 'Temos /milho, /soja e /cafe')

@bot.message_handler(commands=["milho", 'Milho', 'MILHO'])
def milho(message):
    milho= ("Milho= Data: 23/05/2022, Preço: R$88,03")
    aviso= ("Fonte: CEPEA")
    nota= ("*Nota: Valores por saca de 60 kg, à vista, descontado o Prazo de Pagamento pela taxa CDI/CETIP.")
    bot.reply_to(message, milho)
    bot.send_message(message.chat.id,   text=(f'{aviso}\n{nota}'))
        
@bot.message_handler(commands=['help'])
def help(message):
    bot.reply_to(message, "Comandos básicos são: /cotacao e /sair")
    
@bot.message_handler(commands=["soja", 'Soja', 'SOJA'])
def soja(message):
    soja= ("Soja= Data: 23/05/2022, Preço: R$191,68")
    aviso= ("Fonte: CEPEA")
    nota= ("*Nota: Valores por saca de 60 kg, à vista, descontado o Prazo de Pagamento pela taxa CDI/CETIP.")
    bot.reply_to(message, soja,)
    bot.send_message(message.chat.id,   text=(f'{aviso}\n{nota}'))
    
@bot.message_handler(commands=["exit", 'sair'])
def exit(message):
    bot.reply_to(message,"Saindo..")

@bot.message_handler(commands=["cafe", 'café', 'Cafe', 'Café', 'CAFE', 'CAFÉ'])
def cafe(message):
    cafe= ("Café= Data: 23/05/2022 Preco: 1.229,41")
    aviso= ("Fonte: CEPEA")
    nota= ("*Nota: Valores por saca de 60 kg, à vista, descontado o Prazo de Pagamento pela taxa CDI/CETIP.")
    bot.reply_to(message, cafe)
    bot.send_message(message.chat.id,   text=(f'{aviso}\n{nota}'))

bot.polling()

 