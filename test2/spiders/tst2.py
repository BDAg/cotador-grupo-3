import scrapy


class Tst2Spider(scrapy.Spider):
    name = 'tst2'
    start_urls = ['https://www.cepea.esalq.usp.br/br/indicador/milho.aspx','https://www.cepea.esalq.usp.br/br/indicador/soja.aspx', 'https://www.cepea.esalq.usp.br/br/indicador/cafe.aspx']

    def parse(self, response):
        for esalq in response.xpath("//table[@id='imagenet-indicador1']"):
            titulo= esalq.xpath("//*[@id='imagenet-content']/div[2]/div[1]/div[1]/h2//text()").getall()

            hoje= esalq.xpath("//*[@id='imagenet-indicador1']/tbody/tr[1]/td[1]//text()").getall()
            ontem= esalq.xpath("//*[@id='imagenet-indicador1']/tbody/tr[2]/td[1]//text()").getall()
            anteontem= esalq.xpath('//*[@id="imagenet-indicador1"]/tbody/tr[3]/td[1]//text()').getall()
            antesdeanteontem= esalq.xpath('//*[@id="imagenet-indicador1"]/tbody/tr[4]/td[1]//text()').getall()
            antesdeantesdeanteontem= esalq.xpath('//*[@id="imagenet-indicador1"]/tbody/tr[5]/td[1]//text()').getall()
            brl_hoje= esalq.xpath("//*[@id='imagenet-indicador1']/tbody/tr[1]/td[2]//text()").getall()
            brl_ontem= esalq.xpath("//*[@id='imagenet-indicador1']/tbody/tr[2]/td[2]//text()").getall()
            brl_anteontem= esalq.xpath("//*[@id='imagenet-indicador1']/tbody/tr[3]/td[2]//text()").getall()
            brl_antesdeanteontem= esalq.xpath("//*[@id='imagenet-indicador1']/tbody/tr[4]/td[2]//text()").getall()
            brl_antesdeantesdeanteontem= esalq.xpath("//*[@id='imagenet-indicador1']/tbody/tr[5]/td[2]//text()").getall()
            yield{" "}
            yield{"Produto": titulo }
            yield{"Data": hoje,
            "Preco": brl_hoje}
            yield{"Data": ontem,
                "Preco": brl_ontem}
            yield{"Data": anteontem,
                "Preco": brl_anteontem}
            yield{"Data": antesdeanteontem,
                "Preco": brl_antesdeanteontem,}
            yield{"Data": antesdeantesdeanteontem,
                "Preco": brl_antesdeantesdeanteontem} 


